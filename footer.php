<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package jcm
 */

?>
<footer id="footer">
	<div class="container">
		<div class="footer footer1">
			<div>
				<a class="back2top" href="javascript:void(0);"><?php _e('Back to top', 'jcm'); ?></a>
			</div>
			<div class="social-media">
				<?php _e('Find me on:', 'jcm'); ?>
				<?php if(have_rows('social_media', 'option')) : 
					while(have_rows('social_media', 'option')) : the_row(); ?>
						<a href="<?php _e(get_sub_field('social_media_links', 'option')); ?>"><span><?php _e(get_sub_field('social_media_name', 'option')); ?></span></a>
					<?php endwhile;
				endif; ?>
			</div>
		</div><!--.footer1-->

		
		<div class="footer contact">
			<div class="adres"><?php echo get_field('adres', 'option'); ?></div>
			<div class="email"><a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a></div>
			<div class="telp"><a href="tel:<?php echo str_replace(' ', '', str_replace('-', '', get_field('telp', 'option'))) ?>"><?php the_field('telp', 'option'); ?></a></div>
		</div>

		<div class="footer footer2">
			<div class="left">
				<?php
					if ( has_nav_menu('footer') ) : ?>
						<div class="nav">
							<?php plain_menu('footer'); ?>
						</div>
				<?php endif;?>

				<div>
					<span>
						Copyright <?php echo bloginfo('name') . " " . date("Y"); ?>
					</span>
					<span>
						All rights reserved
					</span>
				</div>
			</div>

			<div class="right">	
				<a href="https://www.wappstars.nl/" target="_blank">made by Wappstars</a>
			</div>

		</div><!--footer2-->
	</div><!--.container-->
		
</footer>
<?php 
	wp_footer();
?>
</body>
</html>