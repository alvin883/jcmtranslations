<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gasthoeve
 */
get_header(); ?>

	<div id="content">
        <?php
            if( have_posts() ) :
                while ( have_posts() ) : the_post();
                    
                    get_template_part( 'template-parts/content', 'single');

                endwhile; // End of the loop.
                
            else :
                get_template_part( 'template-parts/content', 'none');
            endif;
		?>
	</div><!-- #content -->

<?php
get_footer();
