<?php
/**
 * Template part for displaying Blog List
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package jcm
 */

get_header(); ?>

<div id="content">
    <div id="single" class="blog">
        <div class="section section-1">
            <div class="bg-jumbo-text">
                <?php if(is_home()) {
                    single_post_title(); 
                }?>
            </div>
            <div class="container">
                <h1>
                  <?php if(is_home()) {
                      single_post_title();
                  }?>
                </h1>
            </div>
        </div>
        
        <div class="section section-2">
            <div class="container">
                <div class="row">
                    <?php if ( have_posts() ) :
                        while ( have_posts() ) : the_post(); ?>
                            <div class="col-12 col-md-6 col-lg-4 column">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="bordered-box">
                                        <h2 class="title">
                                            <?php the_title(); ?>
                                        </h2>
                                        <div class="subtitle">
                                            <h5><?php the_time('F j, Y'); ?></h5>
                                        </div>
                                        <div class="jumbo-dots smaller">...</div>
                                        <div class="content">
                                            <p><?php echo content(40); ?></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile;
                    endif; ?>
                </div><!--.row-->
            </div>
        </div><!--.section-2-->

        <!-- Bigger than 500 px screen -->
        <div class="post-navigation wide">
            <div class="info">
                <?php 
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                ?>
            </div>
            <div class="all-page-number">
                <?php 
                    echo paginate_links(array(
                        'total' => $wp_query->max_num_pages
                    ));
                ?>
            </div>
        </div>
        <!-- Smaller than 500 px screen -->
        <div class="post-navigation mobile">
            <div class="info">
                <?php 
                    echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                ?>
            </div>
            <div class="all-page-number">
                <?php
                    previous_posts_link('&laquo; Previous');
                    next_posts_link('Next &raquo;'); 
                ?>
            </div>
        </div>
        
    </div>
</div>

<?php 
    get_footer();
?>