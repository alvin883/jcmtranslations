<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package jcm
 */

get_header(); ?>

	<div id="content">
        <div id="page-404">
			<div class="section section-1">
				<div class="bg-jumbo-text">
					404 Error !
				</div>
				<div class="container">
					<div class="content">
						<h1>You go too far ...</h1>
						<div class="message">Sorry, we can't find the page you are looking for, you entering empty state .</div>
						<a href="<?php echo get_home_url(); ?>" class="btn">
							Go to Homepage
							<i class="fas fa-arrow-right"></i>
						</a>
					</div>
				</div>
			</div>
        </div><!-- #page-404 -->
	</div><!-- #content-->
	
<?php
get_footer();
