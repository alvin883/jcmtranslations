<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Legals Page
 * @package jcm
 */
get_header();
?>
<?php if(have_posts()) :
    while (have_posts()) : the_post(); ?>
        <div id="content">
            <div id="single">
                <div class="section section-1">
                    <div class="bg-jumbo-text">
                        <?php the_title(); ?>
                    </div>
                    <div class="container">
                        <h1><?php the_title(); ?></h1>
                        <span> <?php _e('Last updated:', 'jcm'); ?> 
                            <?php if(get_the_modified_time() != get_the_time()) { 
                                the_modified_time('F j, Y'); 
                            } ?>
                        </span>
                    </div>
                </div>

                <div class="section section-2">
                    <div class="container">
                        <div class="col-12 col-md-8 mx-auto">
                            <div id="the-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile;
endif; ?>

<?php get_footer(); 
