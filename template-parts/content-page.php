<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package jcm
 */
get_header();
?>

        <div id="content">
            <div id="single">
                <div class="section section-1<?php if( has_post_thumbnail() ){ 
                        echo ' has-thumbnail" style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                    } else {
                        echo '"';
                    } ?>>
                    <div class="container">
                        <h1>
                            <?php the_title(); ?>
                        </h1>
                    </div>
                </div>

                <div class="section section-2">
                    <div class="container">
                        <div class="col-12 col-md-8 mx-auto">
                            <div id="the-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php get_footer(); 