jQuery(document).ready(function($){
    // Target URL (#blablabla) onload
    if( $(window.location.hash).length ){
        $('html,body').animate({
            scrollTop: $(window.location.hash).offset().top
        }, 500, 'swing');
    }
    
    // Target URL (#blablabla) onclick *In same location
    $('a[href^="#"]').click(function(e){
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500, 'swing');
        if( $(this).parents(".navbar-nav") && $(this).parents("nav") ){
            $($(this).parents()[1]).find("li.active").each(function(){ // Find current .active element
                $(this).removeClass("active");
            });
            $(this).parent().addClass("active");
        }
    });
    
    // Scroll To the Next-Section
    $(".arrow").click(function() {
        if($(".section-"+$(this).attr("data-jump")).length ){
        $("html, body").animate({ scrollTop: $(".section-"+$(this).attr("data-jump")).offset().top}, 500);
    }});

    $(".back2top").click(function() {
        $('html,body').animate({
            scrollTop: 0
        }, 500, 'swing');
    });

    
    // Navbar, Toggle small navigation when user scroll
    function checkNavbar(){
        var scroll = $(window).scrollTop();
        if(scroll > 100){
            $("#navbar").addClass("white");
        }else{
            $("#navbar").removeClass("white");
        }
    }
    // check onReady
    checkNavbar();
    
    $(window).scroll(function(){
        var scroll = $(window).scrollTop();
        // check onScroll
        checkNavbar();

        // Animating bg-jumbo-text
        if($(".section-1 .bg-jumbo-text").length && !$("#homepage").length){
            // Universal
            $(".section-1 .bg-jumbo-text").css({
                "transform":"translate(-50%, calc(-50% + " + scroll * .5 + "px))",
                "opacity": 1 - (scroll * 0.0013)
            });
        }else if($(".section-1 .bg-jumbo-text").length && $("#homepage").length){
            // Homepage
            var velocity = scroll * .4;
            $(".section-1 .bg-jumbo-text .right").css({
                "transform":"translate(calc(0% + " + velocity + "px), 0%)",
                "opacity": 1 - (scroll * 0.0033)
            });
            $(".section-1 .bg-jumbo-text .left").css({
                "transform":"translate(calc(0% + " + (0 - velocity) + "px), 0%)",
                "opacity": 1 - (scroll * 0.0033)
            });
        }
    });

    if($("#homepage").length){
        $(".section-3  .slider").slick({
            slidesToShow: 2,
            variableWidth: false,
            autoplay: true,
            arrows: true,
            centerPadding: 0,
            autoplaySpeed: 5000,
            responsive: [
                {
                    breakpoint: 991.98,
                    settings: {
                        slidesToShow: 1
                    }
                },
            ]
        });
        $(".js__slick-next").click(function(){
            $(".section-3 .slider").slick('slickNext');
        });
        $(".js__slick-prev").click(function(){
            $(".section-3 .slider").slick('slickPrev');
        });

        $.fn.scrollAnimation = function(){
            el = $(this);
            if($(window).scrollTop() > $(el).offset().top - ($(window).height() / 2) ){
                $(el).addClass("visible");
                return true;
            }
            return false;
        }

        $(".section-2").scrollAnimation();
        $(".section-3").scrollAnimation();
        $(".section-4").scrollAnimation();
        $(".section-5").scrollAnimation();

        $(window).scroll(function(){
            $(".section-2").scrollAnimation();
            $(".section-3").scrollAnimation();
            $(".section-4").scrollAnimation();
            $(".section-5").scrollAnimation();
        });
    }

    $( ".selectLang select" ).change(function() {
        var url = (document.URL).split("?")[0] + $(this).val();
        location.href = url;
    });

    /** Swipebox */
    $( '.gallery-native,.wp-block-gallery' ).each(function(i){
        var newClass= 'gallery_'+ i;
        $(this).addClass(newClass);
        $('.' + newClass + ' a').swipebox();
    });
});