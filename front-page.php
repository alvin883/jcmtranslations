<?php
/**
 * @package jcm
 * @subpackage theme name here
 * 	
 */
?>

<?php get_header(); ?>

<div id="homepage">
	
	<div id="section-1" class="section section-1">
		<div class="bg-jumbo-text left-right">
			<div class="left">translation</div>
			<div class="right">s & editing</div>
		</div>
		<div class="container">
			<div class="wrapper col-12 col-md-8 mx-auto">
				<div class="row">
					<div class="col-12 col-lg-6 left">
						<?php 
							$images = get_field('logo_', 'option');
							if($images):
						?>
								<img src="<?php echo $images['url']; ?>">
						<?php endif; ?>
					</div>
					<div class="col-12 col-lg-6 right">
						<h1>
							<?php the_field('title_sec1', 'option'); ?>
						</h1>
						<p class="content">
							<?php the_field('explanation', 'option'); ?>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="arrow first" data-jump="2"></div>
	</div>

	<!--SECTION 2 -->
	<?php if(get_field('title_sec2', 'option') && get_field('content_sec2', 'option')) : ?>
		<div id="section-2" class="section section-2">
			<div class="jumbo-dots top">...</div>
			<div class="container">
				<div class="jumbo-dots bottom">;</div>
				<div class="wrapper col-12 col-md-8 mx-auto">
					<div class="title">
						<h2>
							<?php the_field('title_sec2', 'option'); ?>
						</h2>
						<div class="side-title vertical small">
							<h5>
								<?php the_field('sub_title_sec2', 'option'); ?>
							</h5>
						</div>
					</div>
					<div class="content">
						<div class="side-title">
							<h5> 
								<?php the_field('sub_title_sec2', 'option'); ?>
							</h5>
						</div>
						<p><?php the_field('content_sec2', 'option'); ?></p>
					</div>
				</div>
			</div>
			<div class="arrow" data-jump="3"></div>
		</div>
	<?php endif; ?>

	<!-- SECTION 3 -->
	<?php if(get_field('title_sec3', 'option') && get_field('content_sec3', 'option')) : ?>
		<div id="section-3" class="section section-3">
			<div class="jumbo-dots dots-bottom">;</div>
			<div class="container">
				<div class="wrapper col-12 col-md-8 mx-auto">
					<div class="title">
						<h2>
							<?php the_field('title_sec3', 'option'); ?>
						</h2>
						<div class="side-title vertical">
							<h5>
								<?php the_field('sub_title_sec3', 'option'); ?>
							</h5>
						</div>
					</div>
					<div class="content">
						<div class="jumbo-dots top">...</div>
						<p><?php the_field('content_sec3', 'option'); ?></p>
					</div>
					<div class="slider">
						<?php if(have_rows('post_card','option')) :
							while(have_rows('post_card', 'option')) : the_row(); ?>
								<div class="column">
									<div class="bordered-box">
										<h3 class="title">
											<?php the_sub_field('title_postcard', 'option'); ?>
										</h3>
										<div class="subtitle">
											<?php the_sub_field('sub_title_postcard', 'option'); ?>
										</div>
										<div class="jumbo-dots smaller">...</div>
										<div class="content">
											<p><?php the_sub_field('content_postcard', 'option'); ?></p>
										</div>
									</div>
								</div>
							<?php endwhile;
						endif; ?>
					</div>
					<div class="bottom">
						<div class="left">
							<button class="js__slick-prev"></button>
							<div class="divider"></div>
							<button class="js__slick-next"></button>
						</div>
						<div class="right">
							<?php _e('Can\'t find what you need ?', 'jcm'); ?> <a href="#section-5"><?php _e('Contact Us', 'jcm'); ?></a>
						</div>
					</div>
				</div>
			</div>
			<div class="arrow" data-jump="4"></div>
		</div>
	<?php endif; ?>
	
	<!--SECTION 4 -->
	<?php if(get_field('title_sec4', 'option') && get_field('content_sec4', 'option')) : ?>
		<div id="section-4" class="section section-4">
			<div class="container">
				<div class="wrapper col-12 col-md-8 mx-auto">
					<div class="title">
						<h2>
							<?php the_field('title_sec4', 'option'); ?>
						</h2>
						<div class="jumbo-dots">...</div>
					</div>
					<div class="content">
						<div class="side-title">
							<h5>
								<?php the_field('sub_title_sec4', 'option'); ?>
							</h5>
						</div>

						<ul class="list">
							<?php 
								if(have_rows('content_sec4', 'option')):
									while(have_rows('content_sec4', 'option')) : the_row(); ?>
										<li>
											<?php the_sub_field('list_content_sec4', 'option'); ?>
										</li>
									<?php endwhile;
								endif;
							?>
						</ul>
					</div>
				</div>
			</div>
			<div class="arrow" data-jump="5"></div>
		</div>
	<?php endif; ?>
	
	<!-- SECTION 5-->
	<?php if(get_field('title_sec5', 'option') && get_field('content_sec5', 'option')) :?>
		<div id="section-5" class="section section-5" id="section-5">
			<div class="container">
				<div class="wrapper col-12 col-md-8 mx-auto">
					<h2 class="title"><?php the_field('title_sec5', 'option'); ?></h2>
					<div class="content">
						<p><?php the_field('content_sec5', 'option'); ?></p>
						<div class="bottom row">
							<div class="col-12 col-md-6">
								<?php _e('Call me:', 'jcm');?> <span class="data"><a href="tel:<?php echo str_replace(' ', '', str_replace('-', '', get_field('telp', 'option'))) ?>"><?php the_field('telp', 'option'); ?></a></span>
							</div>
							<div class="col-12 col-md-6">
								<?php _e('Write me:', 'jcm'); ?> <span class="data"><a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a></span>
							</div>
						</div>
					</div>
					<h4 class="title"> <?php _e('Quick Message?', 'jcm'); ?> </h4>
					<div id="the-content">
						<?php echo do_shortcode(get_field('form_sec5', 'option')); ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

</div>

<?php 
get_footer();
